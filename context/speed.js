let convertMph = (number) => {
    let milesPerHour = parseFloat(number);
    let metersPerSecond = milesPerHour * 0.44704;
    let kilometersPerHour = milesPerHour * 1.609344;

    let result = {
        "miles/Hour": Intl.NumberFormat('en-US').format(milesPerHour),
        "meters/Second": Intl.NumberFormat('en-US').format(metersPerSecond),
        "kilometers/Hour": Intl.NumberFormat('en-US').format(kilometersPerHour)
    };

    return result;
}

let convertMetersPerSecond= (number) => {
    let metersPerSecond = parseFloat(number);
    let milesPerHour = metersPerSecond * 2.2369;
    let kilometersPerHour = metersPerSecond * 3.6;

    let result = {
        "miles/Hour": Intl.NumberFormat('en-US').format(milesPerHour),
        "meters/Second": Intl.NumberFormat('en-US').format(metersPerSecond),
        "kilometers/Hour": Intl.NumberFormat('en-US').format(kilometersPerHour)
    };

    return result;
}

let convertKilometersPerHour = (number) => {
    let kilometersPerHour = parseFloat(number);
    let milesPerHour = kilometersPerHour * 0.6214;
    let metersPerSecond = milesPerHour * 0.44704;

    let result = {
        "miles/Hour": Intl.NumberFormat('en-US').format(milesPerHour),
        "meters/Second": Intl.NumberFormat('en-US').format(metersPerSecond),
        "kilometers/Hour": Intl.NumberFormat('en-US').format(kilometersPerHour)
    };

    return result;
}

module.exports = {
    convertMph,
    convertMetersPerSecond,
    convertKilometersPerHour
}