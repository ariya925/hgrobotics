let convertFlOz = (number) => {
    let flOz = parseFloat(number);
    let cmPowerTree = flOz * 0.000029574;
    let ml = flOz * 29.5735296;

    let result = {
        "fl oz": Intl.NumberFormat('en-US').format(flOz),
        "cmPowerTree": Intl.NumberFormat('en-US').format(cmPowerTree),
        "ml": Intl.NumberFormat('en-US').format(ml)
    };

    return result;
}

let convertCmPowerTree = (number) => {
    let cmPowerTree = parseFloat(number);
    let flOz = cmPowerTree * 29.57353;
    let ml = cmPowerTree * 1;

    let result = {
        "fl oz": Intl.NumberFormat('en-US').format(flOz),
        "cmPowerTree": Intl.NumberFormat('en-US').format(cmPowerTree),
        "ml": Intl.NumberFormat('en-US').format(ml)
    };

    return result;
}

let convertMl = (number) => {
    let ml = parseFloat(number);
    let flOz = ml * 0.0338149227;
    let cmPowerTree = ml;

    let result = {
        "fl oz": Intl.NumberFormat('en-US').format(flOz),
        "cmPowerTree": Intl.NumberFormat('en-US').format(cmPowerTree),
        "ml": Intl.NumberFormat('en-US').format(ml)
    };

    return result;
}


module.exports = {
    convertFlOz,
    convertCmPowerTree,
    convertMl
}