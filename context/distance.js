let convertMile = (number) => {
    let mile = parseFloat(number);
    let metre = mile * 1_609.344;
    let wa = metre * 0.25;

    let result = {
        "mile": Intl.NumberFormat('en-US').format(mile),
        "metre": Intl.NumberFormat('en-US').format(metre),
        "wa(วา)": Intl.NumberFormat('en-US').format(wa)
    };

    return result;
}

let convertMetre = (number) => {
    let metre = parseFloat(number);
    let mile = metre * 0.00062137;
    let wa = metre * 0.5;

    let result = {
        "mile": Intl.NumberFormat('en-US').format(mile),
        "metre": Intl.NumberFormat('en-US').format(metre),
        "wa(วา)": Intl.NumberFormat('en-US').format(wa)
    };

    return result;
}

let convertWa = (number) => {
    let wa = parseFloat(number);
    let metre = wa * 2;
    let mile = metre * 0.00062137;

    let result = {
        "mile": Intl.NumberFormat('en-US').format(mile),
        "metre": Intl.NumberFormat('en-US').format(metre),
        "wa(วา)": Intl.NumberFormat('en-US').format(wa)
    };

    return result;
}

module.exports = {
    convertMile,
    convertMetre,
    convertWa
}