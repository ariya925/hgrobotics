let convertAcre = (number) => {
    let acre = parseFloat(number);
    let mPowerTwo = acre * 4_046.856422;
    let rai = acre * 2.529;

    let result = {
        "acre": Intl.NumberFormat('en-US').format(acre),
        "mPowerTwo": Intl.NumberFormat('en-US').format(mPowerTwo),
        "rai(ไร่)": Intl.NumberFormat('en-US').format(rai)
    };

    return result;
}

let convertMPowerTwo = (number) => {
    let mPowerTwo = number;
    let acre = mPowerTwo * 4_047;
    let rai = mPowerTwo * 0.000625;

    let result = {
        "acre": Intl.NumberFormat('en-US').format(acre),
        "mPowerTwo": Intl.NumberFormat('en-US').format(mPowerTwo),
        "rai(ไร่)": Intl.NumberFormat('en-US').format(rai)
    };

    return result;
}

let convertRai = (number) => {
    let rai = number;
    let mPowerTwo = rai * 1_600;
    let acre = rai * 0.3954;

    let result = {
        "acre": Intl.NumberFormat('en-US').format(acre),
        "mPowerTwo": Intl.NumberFormat('en-US').format(mPowerTwo),
        "rai(ไร่)": Intl.NumberFormat('en-US').format(rai)
    };

    return result;
}

module.exports = {
    convertAcre,
    convertMPowerTwo,
    convertRai
}