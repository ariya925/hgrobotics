var express = require('express');
var router = express.Router();

const { convertAcre, convertMPowerTwo, convertRai } = require('../function/context/area');
const { convertMile, convertMetre, convertWa } = require('../function/context/distance');
const { convertMph, convertMetersPerSecond, convertKilometersPerHour } = require('../function/context/speed');
const { convertFlOz, convertCmPowerTree, convertMl } = require('../function/context/volume');

router.get('/context/:context/measurement_system/:measurement_system/unit/:unit/number/:number', (req, res, next) => {
  let { context, measurement_system, unit, number } = req.params

  if (context, measurement_system, unit, number !== undefined && !isNaN(number) && isNaN(context, measurement_system, unit)) {

    if (context === 'area') {
      if (measurement_system.toLocaleLowerCase() === 'imperial') {
        var result = convertAcre(number);
      } else if (measurement_system.toLocaleLowerCase() === 'metric') {
        var result = convertMPowerTwo(number);
      } else if (measurement_system.toLocaleLowerCase() === 'customized') {
        var result = convertRai(number);
      } else {
        res.statusCode = 400;
        res.send({
          "message": "unit invalid"
        })
      }
      res.statusCode = 200;
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Headers', '*');
      res.send(result)
    } else if (context === 'distance') {
      if (measurement_system.toLocaleLowerCase() === 'imperial') {
        var result = convertMile(number);
      } else if (measurement_system.toLocaleLowerCase() === 'metric') {
        var result = convertMetre(number);
      } else if (measurement_system.toLocaleLowerCase() === 'customized') {
        var result = convertWa(number);
      } else {
        res.statusCode = 400;
        res.send({
          "message": "units invalid"
        })
      }
      res.statusCode = 200;
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Headers', '*');
      res.send(result)
    } else if (context === 'volume') {
      if (measurement_system.toLocaleLowerCase() === 'imperial') {
        var result = convertMph(number);
      } else if (measurement_system.toLocaleLowerCase() === 'metric') {
        var result = convertMetersPerSecond(number);
      } else if (measurement_system.toLocaleLowerCase() === 'customized') {
        var result = convertKilometersPerHour(number);
      } else {
        res.statusCode = 400;
        res.send({
          "message": "units invalid"
        })
      }
      res.statusCode = 200;
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Headers', '*');
      res.send(result)
    } else if (context === 'speed') {
      if (measurement_system.toLocaleLowerCase() === 'imperial') {
        var result = convertFlOz(number);
      } else if (measurement_system.toLocaleLowerCase() === 'metric') {
        var result = convertCmPowerTree(number);
      } else if (measurement_system.toLocaleLowerCase() === 'customized') {
        var result = convertMl(number);
      } else {
        res.statusCode = 400;
        res.send({
          "message": "units invalid"
        })
      }
      res.statusCode = 200;
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Headers', '*');
      res.send(result)
    } else {
      res.statusCode = 400;
      res.send({
        "message": "measurement_system invalid"
      })
    }

  } else {
    res.statusCode = 400;
    res.send({
      "message": "number invalid"
    })
  }

});

module.exports = router;
