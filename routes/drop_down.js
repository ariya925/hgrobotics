var express = require('express');
var router = express.Router();

router.get('/context', (req, res, next) => {

    let data = ['area', 'distance', 'volume', 'speed'];
    res.statusCode = 200;
    res.set('Access-Control-Allow-Origin', '*');
    res.send({
        data
    })
});

router.get('/measurement_system', (req, res, next) => {

    let data = ['imperial', 'metric', 'customized'];
    res.statusCode = 200;
    res.set('Access-Control-Allow-Origin', '*');
    res.send({
        data
    })
});

router.get('/context/:name/untis', (req, res, next) => {
    let { name } = req.params

    if (name !== undefined && isNaN(name)) {
        let data = [];
        if (name === 'area') {
            data.push('acre', 'm2', 'rai');
        } else if (name === 'distance') {
            data.push('mile', 'metre', 'wa');
        } else if (name === 'volume') {
            data.push('floz', 'cm3', 'ml');
        } else if (name === 'speed') {
            data.push('mph', 'm/s', 'km/h');
        }
        res.statusCode = 200;
        res.set('Access-Control-Allow-Origin', '*');
        res.send({
            data
        })
    } else {
        res.statusCode = 400;
        res.send({
            "message": "number invalid"
        })
    }

});

module.exports = router;
